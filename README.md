#entact_driver

##Computer setup
Set Device name in connection to the exact name "enx8cae4ce9a9b0" for example

change the string in w4_api.cpp to the same

set ipv6/4 settings to addresses only

##Running
Changing the socket to only use network card connected to entact router requires sudo. Sudo in ROS is tricky so this node requires the following code to run.
```
sudo su
source /opt/ros/kinetic/setup.bash
source catkin_ws/devel/setup.bash
rosrun entact_driver entact_driver_node 
```