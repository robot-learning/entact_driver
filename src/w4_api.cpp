// --------------------------------------------------------------------------------------
// Module       : W4_API
// Description  : 
// Author       : Entact Robotics Inc.
// Revision     : 1.0
// --------------------------------------------------------------------------------------
// Entact Robotics inc. www.entactrobotics.com
// --------------------------------------------------------------------------------------
//
// Change Log
// November 1, 2015: RL: Revision 1.0
//
//
// TO DO:
// 

#define WIN32_LEAN_AND_MEAN // Reducing the number of included header files in windows.h
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>    //for error handling


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <iostream>
#include <time.h>

#include "w4_api.h"

#define		W4_MAX_DEV				(16) // Maximum number of allowable devices 

#define		W4_RETURN_OK			(1)   // Success
#define		W4_RETURN_ERROR			(-1)  // Failure
#define		W4_RETURN_ARGERR		(-2)  // Invalid Function Argument
#define		W4_RETURN_UDPERR		(-3)  // UDP Initialization Error
#define		W4_RETURN_IPARGERR		(-4)  // Invalid IP Argument Error
#define		W4_RETURN_SOCKERR		(-5)  // Socket Initialization Error
#define		W4_RETURN_BINDERR		(-6)  // Bind Error
#define		W4_RETURN_TOUTERR		(-7)  // Set Socket Time-out Initialization Error
#define		W4_RETURN_SENDERR		(-8)  // UDP Send Error
#define		W4_RETURN_RECVERR		(-9)  // UDP Receive Error
#define		W4_RETURN_CMDFAIL		(-10) // Command to W4 Device has failed
#define		W4_RETURN_BCSTERR		(-11) // Socket Broadcast initialization error 
#define		W4_RETURN_HNDLERR		(-12) // Invalid Handle Argument 
#define		W4_RETURN_INITERR		(-13) // API requires initialization

#define W4_APIS_BCAST_PORT_BASE		(55500)	// Port the API uses for listening after a network broadcast (device discovery)
#define W4_ROBOTS_LISTEN_PORT		(55556) // Port the W4 devices use for listening  
#define W4_APIS_LISTEN_PORT_BASE	(55596) // Port the API uses for listening to each attached device (port increments by one per device)
#define W4_RECV_TIMEOUT				(1000)	// milli-seconds (different units in linux)

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1


// Global Data ********************************

typedef enum
{
  W4_UNCONNECTED,
  W4_CONNECTED
} w4_connected_t;

typedef struct {
	unsigned int sd;
	struct sockaddr_in w4_address;		// The devices listening IP/port for incoming packets
	struct sockaddr_in api_address;		// The API listening IP/port for incoming packets
	w4_connected_t w4_connected;		// set if device is connected
} w4_device_t;

w4_device_t device_list[W4_MAX_DEV];

int n_devices = 0; // Number of devices discovered during openW4() call

typedef enum
{
  UNINITIALIZED,
  INITIALIZED
} api_init_t;

api_init_t api_initialized = UNINITIALIZED;	// Used to indicate if another thread (in this same process) has already initialized the API
api_init_t api_discover = UNINITIALIZED;


#pragma data_seg(".shared") // Data shared across multiple instances of the .DLL (occurs when different processes access the .DLL)
	u_short w4_startport = W4_APIS_LISTEN_PORT_BASE;	// port base for the API (every new discovered W5D increments this value)
	u_short bc_startport = W4_APIS_BCAST_PORT_BASE; // with every new API instance (and call to openW5D()) this value increments
#pragma data_seg()
// Remember to have Linker "Specify Section Attributes" set as .shared,RWS


// Private Functions ********************************

int init_udp(); 
int uninit_udp();
int discover_devices(int *n_devices);
int validate_handle(void *handle);


//*****************************************************************************************
// openW4()
//*****************************************************************************************
int openW4()
{
	if (api_initialized == INITIALIZED) return W4_RETURN_OK; // Another thread (in this process) has already initialized the API, copy over the handles

	// Initialize global data structures
	n_devices = 0; // Initialize number of discovered devices to 0
	memset((void*) &device_list, 0, sizeof(device_list)); // Clear Device Structure
	for (int i = 0; i < W4_MAX_DEV; i++) device_list[i].w4_connected = W4_UNCONNECTED; // Initialize structure elements to unconnected
	
	// Initialize the api's udp socket
	if (!init_udp()) return W4_RETURN_UDPERR; 

	api_initialized = INITIALIZED;
	
	return W4_RETURN_OK;
}

//*****************************************************************************************
// closeW4()
//*****************************************************************************************
int closeW4()
{
	if (!uninit_udp()) return W4_RETURN_UDPERR; // uninitialize the api's udp socket

	api_initialized = UNINITIALIZED;

	return W4_RETURN_OK;
}

//*****************************************************************************************
// discoverW4()
//*****************************************************************************************
int discoverW4(void * handles[], int length)
{
	if (length <= 0) return W4_RETURN_ARGERR; // Verify length is correct
	if (api_initialized == UNINITIALIZED) return W4_RETURN_INITERR;
	
	if (api_discover == INITIALIZED) // Another thread (in this process) has already discovered devices on the network, copy over the handles 
	{
		for (int i = 0; i < n_devices; i++) 
		{
			if (i < length) handles[i] = (void*) &device_list[i];
		}

		return n_devices;
	}
	
	

	// broadcast onto the local network in search of entact devices
	// fill the device_list[] array with IP/port and device information
	printf("discovering\n");
	int rc = discover_devices(&n_devices);
	printf("finished discovering %d \n",rc);

	if (rc != W4_RETURN_OK) return rc; 
	
	int i = 0;
	while (i < n_devices)	// provide users handle array with pointers to newly discovered devices
	{
		if (i < length) handles[i] = (void *) &device_list[i];
		i++;
	}
	printf("%d devices discovered \n",n_devices );
	api_discover = INITIALIZED; 

	return n_devices;
}

//*****************************************************************************************
// connectW4()
//*****************************************************************************************
int connectW4(void * handles[], int index, char ip_address[])
{
	if (index < 0) return W4_RETURN_ARGERR;
	if (index > W4_MAX_DEV) return W4_RETURN_ARGERR;
	if (api_initialized == UNINITIALIZED) return W4_RETURN_INITERR;

	// Convert IP address
	if (inet_pton(AF_INET, ip_address, &device_list[index].w4_address.sin_addr) == 1) // successfull conversion to binary form
	{
		// setting up the destination port (IP address assigned from function argument)
		device_list[index].w4_address.sin_port = htons(W4_ROBOTS_LISTEN_PORT); 
		device_list[index].w4_address.sin_family = AF_INET;
		
		// setting up the listening port for the API
		if (w4_startport > (W4_APIS_LISTEN_PORT_BASE+100)) w4_startport = W4_APIS_LISTEN_PORT_BASE; // cap startport to a maximum value
		device_list[index].api_address.sin_port = htons(w4_startport++);
		device_list[index].api_address.sin_addr.s_addr = htonl(INADDR_ANY);
		device_list[index].api_address.sin_family = AF_INET;

		// setting up local socket descriptor
		device_list[index].sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (device_list[index].sd == INVALID_SOCKET) return W4_RETURN_SOCKERR;

		// binding the listening socket
		if (bind(device_list[index].sd, (const sockaddr *) &device_list[index].api_address, sizeof(sockaddr_in)) == SOCKET_ERROR) return W4_RETURN_BINDERR;

		// setting the recv socket with a timeout
		unsigned int w4_recv_timeout = W4_RECV_TIMEOUT; // milli-seconds
		if (setsockopt(device_list[index].sd, SOL_SOCKET, SO_RCVTIMEO, (char *) &w4_recv_timeout, sizeof(unsigned int)) ==  SOCKET_ERROR) return W4_RETURN_TOUTERR;

	}
	else // unsuccessfull conversion of IP address
	{
		return W4_RETURN_IPARGERR;
	}

	// Communication to test if a device is actually there
	udp_hdr_t outpkt;
	outpkt.command = W4_CMD_NULL;
	outpkt.return_command = W4_RCMD_STATUS;
	outpkt.length = 0;
	int pkt_size = sizeof(udp_hdr_t);
	if(sendto(device_list[index].sd, (const char*) &outpkt, pkt_size, 0, (sockaddr *) &device_list[index].w4_address, sizeof(sockaddr_in)) == SOCKET_ERROR) return W4_RETURN_SENDERR;

	// Wait for response
	udp_outpkt_status_t statuspkt;
	int rec_len = recvfrom(device_list[index].sd, (char*) &statuspkt, sizeof(statuspkt), 0, NULL, NULL);
	if (rec_len == SOCKET_ERROR) return W4_RETURN_RECVERR; // Network setup succeeded, but no W4 found at IP address
	if (statuspkt.header.command != (W4_CMD_NULL|W4_CMD_ACK)) return W4_RETURN_CMDFAIL;
	
	// Pass back pointer to the handles array
	handles[index] = (void *) &device_list[index];
	device_list[index].w4_connected = W4_CONNECTED;

	return W4_RETURN_OK;	
}

//*****************************************************************************************
// getipW4()
//*****************************************************************************************
int getipW4(void * handle, char ip_address[])
{
	// Validate handle
	if (validate_handle(handle) != W4_RETURN_OK) return W4_RETURN_HNDLERR;

	const char *ip = NULL;
	char ip_str[256];

	ip = inet_ntop(AF_INET, &(((w4_device_t *) handle)->w4_address.sin_addr), ip_str, 256);

	memcpy(ip_address, ip_str, 256);

	return W4_RETURN_OK;
}

//*****************************************************************************************
// sendW4()
//*****************************************************************************************
int sendW4(void * handle, int command, int return_command, void * sbuf, int s_len, void * rbuf, int r_len)
{
	// Validate handle
	if (validate_handle(handle) != W4_RETURN_OK) return W4_RETURN_HNDLERR;

	// Validate sending buffer length
	if (s_len > (sizeof(udp_pkt_t) - sizeof(udp_hdr_t))) s_len = sizeof(udp_pkt_t) - sizeof(udp_hdr_t); // No larger than maximum packet data length
	else if (s_len < 0) s_len = 0; // Non negative length

	// Prepare send packet
	udp_pkt_t spkt;
	spkt.header.command = command;
	spkt.header.return_command = return_command;
	spkt.header.length = s_len;
	memcpy((void *) &spkt.data, (void *) sbuf, s_len);

	// UDP Send 
	int pkt_size = sizeof(udp_hdr_t) + s_len;
	int rc = sendto(((w4_device_t *) handle)->sd, (const char*) &spkt, pkt_size, 0, (struct sockaddr *) &((w4_device_t *) handle)->w4_address, sizeof(sockaddr_in));
	if (rc == SOCKET_ERROR) return W4_RETURN_SENDERR;

	// UDP Receive
	udp_pkt_t rpkt;
	int rec_len = recvfrom(((w4_device_t *) handle)->sd, (char*) &rpkt, sizeof(udp_pkt_t), 0, NULL, NULL);
	
	// Verify received packets length
	int payload_len = rec_len - sizeof(udp_hdr_t);
	if (payload_len < 0) return W4_RETURN_RECVERR;	// Less than minimum packet size (header length) returned
	if (payload_len != rpkt.header.length) return W4_RETURN_RECVERR; // Packet size recieved does not match what packet claims
	if (payload_len > (sizeof(udp_pkt_t) - sizeof(udp_hdr_t))) return W4_RETURN_RECVERR; // Packet data size is too large (and probably corrupt)

	// Verify our command has been acknowledged by the W4
	if (rpkt.header.command != (command|W4_CMD_ACK)) return W4_RETURN_CMDFAIL;

	// Verify receiving buffer length
	if (r_len < 0) r_len = 0; // only ensuring the buffer is greater or equal to 0 length, not concerned if its made too large for some reason

	// Copy out the received packet data
	if (payload_len < r_len) memcpy((void *) rbuf, (void *) &rpkt.data, payload_len);
	else memcpy((void *) rbuf, (void *) &rpkt.data, r_len);

	return W4_RETURN_OK;
}

//*****************************************************************************************
// init_udp()
//*****************************************************************************************
int init_udp()
{
	return W4_RETURN_OK;
}

//*****************************************************************************************
// uninit_udp()
//*****************************************************************************************
int uninit_udp()
{
	return W4_RETURN_OK;
}

//*****************************************************************************************
// discover_devices()
//*****************************************************************************************
int discover_devices(int *n_devices)
{
	int pkt_size, rec_len;
	unsigned int sockaddr_size;
	// Setting up a broadcast socket
	unsigned int sd;
	sockaddr_in broadcast_address;
	sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sd == INVALID_SOCKET) return W4_RETURN_SOCKERR;
	
	// Broadcast listening address
	broadcast_address.sin_family = AF_INET;
	broadcast_address.sin_port = htons(bc_startport);
	broadcast_address.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (const sockaddr *) &broadcast_address, sizeof(sockaddr_in)) == SOCKET_ERROR) return W4_RETURN_BINDERR; //-bc_startport;
	bc_startport++; // increment broadcast port (for other instances of the API)

	// Broadcast time-out configuration
	bool bOptVal = true;

	struct timeval tv;
	tv.tv_sec = W4_RECV_TIMEOUT / 1000.0;
	tv.tv_usec = 0;

	char *interface = "enx8cae4ce9a9b0";
	printf("connecting");
	if(setsockopt(sd,SOL_SOCKET,SO_BINDTODEVICE,interface,strlen(interface)) == SOCKET_ERROR){
		printf("new error is %d \n",errno);
	}

	if (setsockopt(sd, SOL_SOCKET, SO_BROADCAST, (char *) &bOptVal, sizeof(unsigned int)) ==  SOCKET_ERROR) return W4_RETURN_BCSTERR;		// temporarily setting the send socket to broadcast mode
	if (setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof(tv)) ==  SOCKET_ERROR){
	 printf("error is %d \n",errno);
	 return W4_RETURN_TOUTERR;	// setting the recv socket with a timeout
	
	}
	// Broadcast sending address
	memset(&broadcast_address, '\0', sizeof(struct sockaddr_in));
    broadcast_address.sin_family = AF_INET;
    broadcast_address.sin_port = htons(W4_ROBOTS_LISTEN_PORT);
    broadcast_address.sin_addr.s_addr = htonl(INADDR_BROADCAST); 
	
	// Broadcast packet (status request packet to each W4)
	udp_hdr_t outpkt;
	outpkt.command = W4_CMD_NULL;
	outpkt.return_command = W4_RCMD_STATUS;
	outpkt.length = 0;
	pkt_size = sizeof(udp_hdr_t);

	// Broadcast sendto()
    if(sendto(sd, (const char*) &outpkt, pkt_size, 0, (sockaddr *) &broadcast_address, sizeof(sockaddr_in)) == SOCKET_ERROR) return W4_RETURN_SENDERR;
	
	// Loop through all the packets we've received back
	int valid_msg = 1;
	int dev_found = 0;
	sockaddr_in device_address;
	udp_pkt_t statuspkt; 
	while (valid_msg)
	{
		sockaddr_size = sizeof(struct sockaddr);
		rec_len = recvfrom(sd, (char*) &statuspkt, sizeof(statuspkt), 0, (struct sockaddr *) &device_address, &sockaddr_size);
		if (rec_len != SOCKET_ERROR)
		{
			// recording the IP address and port information for the W5D's listening port
			device_list[dev_found].w4_address.sin_port = htons(W4_ROBOTS_LISTEN_PORT); 
			device_list[dev_found].w4_address.sin_addr.s_addr = device_address.sin_addr.s_addr;
			device_list[dev_found].w4_address.sin_family = AF_INET;

			// recording the IP address and port information for the API's listening port (one port per attached device)
			device_list[dev_found].api_address.sin_port = htons(w4_startport);
			device_list[dev_found].api_address.sin_addr.s_addr = htonl(INADDR_ANY);
			device_list[dev_found].api_address.sin_family = AF_INET;

			// creating the W5D's socket
			device_list[dev_found].sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			if (device_list[dev_found].sd == INVALID_SOCKET) return W4_RETURN_SOCKERR;

			// binding the W5D's socket to the API's listening port
			if (bind(device_list[dev_found].sd, (const sockaddr *) &device_list[dev_found].api_address, sizeof(sockaddr_in)) == SOCKET_ERROR) return W4_RETURN_BINDERR;

			// setting a recieve timeout for recvfrom() calls on devices listening socket
			if (setsockopt(device_list[dev_found].sd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof(tv)) ==  SOCKET_ERROR) return W4_RETURN_TOUTERR;

			// setting the device status to connected
			device_list[dev_found].w4_connected = W4_CONNECTED;

			dev_found++;
			w4_startport++;
		}
		else
		{
			valid_msg = 0;
		}
	} 
	
	*n_devices = dev_found;

	// disabling broadcast mode on the send socket
	bOptVal = 0;
	printf("disableing \n");
	if (setsockopt(sd, SOL_SOCKET, SO_BROADCAST, (char *) &bOptVal, sizeof(unsigned int)) ==  SOCKET_ERROR) return W4_RETURN_BCSTERR;

	return W4_RETURN_OK;
}

//*****************************************************************************************
// validate_handle()
//*****************************************************************************************
int validate_handle(void *handle)
{
	if ( handle == NULL ) return W4_RETURN_ARGERR; // handle pointer is uninitialized
	if ( ((w4_device_t *) handle)->w4_connected != W4_CONNECTED) return W4_RETURN_ARGERR; 
	
	return W4_RETURN_OK;
}
