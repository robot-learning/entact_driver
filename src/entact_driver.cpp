#include "ros/ros.h"
#include <sensor_msgs/JointState.h>
#include "w4_api.h"
#include <ros/console.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseStamped.h>

#include <std_msgs/Float64MultiArray.h>
#define DISABLED_MODE 0
#define POSITION_MODE 1
#define FORCE_MODE 2
#define TORQUE_MODE 3

int mode = DISABLED_MODE;
int driverMode = DISABLED_MODE;

char ip_address[256] = "0.0.0.0\n";
taskpos_t w4_taskpos;	// position of the W4											
void * handles[16];		// handle for W4

//0 disabled, 1 position, 2 force
int initialize_W4_disabled()
{
	// Initialize the W5D API ********
	int rc = openW4();
	if (rc < 0) 
	{ 
		ROS_INFO("Unable to open W4 API.");
		return 0;
	}

	rc = discoverW4(handles, 16);
	if (rc < 1) 
	{
		ROS_INFO("Unable to find Entact device.");
		return 0;
	}

	// Check if device has been homed *********
	status_t w4_status;
	rc = sendW4(handles[0], W4_CMD_NULL, W4_RCMD_STATUS, NULL, 0, &w4_status, sizeof(w4_status));
	if (rc < 1) 
	{
		ROS_INFO("Communication error while checking homed status.");
		return 0;
	}
	if (w4_status.homed != 1)
	{
		ROS_INFO("Entact Device requires homing.");
		return -1;
	}

	// Disable the W5D *********
	rc = sendW4(handles[0], W4_CMD_STATE|W4_DISABLED, W4_RCMD_NULL, NULL, 0, NULL, 0);
	if (rc != 1) 
	{
		ROS_INFO("Unable to disable the Entact Device Here.");
		return 0;
	}
	return 1;
}

// have had no success making this actually work
void poseCallBack(const geometry_msgs::PoseStamped::ConstPtr& msg){
	int rc;
	float pos[12];// = {0,0,0,0,0,0,0,0,0,0,0,0};
	float PG = 2000.0;
	float DG = 8.0;
	float gains[16] = {PG,DG,PG,DG,PG,DG,PG,DG,PG,DG,PG,DG,0,0,0,0};
	float current_pos[18];

	taskpos_t w4_taskpos_ht;
	ROS_INFO("got msg ");

	if(mode != POSITION_MODE){
		rc = sendW4(handles[0], W4_CMD_STATE|W4_POSITION_CONTROL, W4_RCMD_NULL, &gains, sizeof(gains), NULL, 0);
		if(rc < 1){
			ROS_INFO("error enabling position control ");
		}else{
			ROS_INFO("succeed enabling position control ");
		}
		mode = POSITION_MODE;
	}
	pos[0] = msg->pose.position.x * 1000;
	pos[1] = msg->pose.position.y * 1000;
	pos[2] = msg->pose.position.z * 1000;
	pos[4] = -1;
	pos[6] = 1;
	pos[11] = 1; 

	ROS_INFO("%3.3f %3.3f %3.3f",pos[0],pos[1],pos[2]);
	
	rc = sendW4(handles[0], W4_CMD_SETPOINT|W4_POSITION, W4_RCMD_TASKPOS, &pos, sizeof(pos),&current_pos , sizeof(current_pos));
	if(rc < 1){
		ROS_INFO("error sending position ");
	}else{
		ROS_INFO("position %f, %f, %f",current_pos[0],current_pos[1],current_pos[2]);
		ROS_INFO("orientation1 %f, %f, %f",current_pos[3],current_pos[4],current_pos[5]);
		ROS_INFO("orientation2 %f, %f, %f",current_pos[6],current_pos[7],current_pos[8]);
		ROS_INFO("orientation3 %f, %f, %f",current_pos[9],current_pos[10],current_pos[11]);
		rc = sendW4(handles[0], W4_CMD_SETPOINT|W4_POSITION, W4_RCMD_NULL, &current_pos, sizeof(current_pos),NULL , 0);
	}
}

void forceCallBack(const geometry_msgs::TwistStamped::ConstPtr& msg){
	int rc;
	float force[6];

	if(mode != FORCE_MODE){
		rc = sendW4(handles[0], W4_CMD_STATE|W4_FORCE_CONTROL, W4_RCMD_NULL, NULL, 0, NULL, 0);
		if(rc < 1){
			ROS_INFO("error enabling force control ");
		}else{
			ROS_INFO("succeed enabling force control ");
		}
		mode = FORCE_MODE;
	}
	force[0] = msg->twist.linear.x;
	force[1] = msg->twist.linear.y;
	force[2] = msg->twist.linear.z;

	force[4] = msg->twist.angular.x * 1000;
	force[5] = msg->twist.angular.y * 1000;
	force[6] = msg->twist.angular.z * 1000;
	
	rc = sendW4(handles[0], W4_CMD_SETPOINT|W4_FORCE,W4_RCMD_NULL , &force, sizeof(force), NULL, 0);
	if(rc < 1){
		ROS_INFO("error sending force ");
	}	
}

void torqueCallBack(const std_msgs::Float64MultiArray::ConstPtr& msg){
	int rc;
	float torque[6];

	if(mode != TORQUE_MODE){
		rc = sendW4(handles[0], W4_CMD_STATE|W4_TORQUE_CONTROL, W4_RCMD_NULL, NULL, 0, NULL, 0);
		if(rc < 1){
			ROS_INFO("error enabling torque control ");
		}else{
			ROS_INFO("succeed enabling torque control ");
		}
		mode = TORQUE_MODE;
	}
	for( int i = 0; i<6; i ++){
		torque[i] = msg->data[i];
	}

	rc = sendW4(handles[0], W4_CMD_SETPOINT|W4_TORQUE,W4_RCMD_NULL , &torque, sizeof(torque), NULL, 0);
	if(rc < 1){
		ROS_INFO("error sending torque ");
	}
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "entact");
	ros::NodeHandle n;
	ros::Rate loop_rate(1000);
	tf::TransformBroadcaster br;
	tf::Transform transform;
	sensor_msgs::JointState js;
  	float tau[8];

	js.name.resize(8,"joint");
	js.position.resize(8,0);
	js.velocity.resize(8,0);
	js.effort.resize(8,0);

	int rc;
	bool debug = false;
	taskpos_t w4_taskpos_ht;
	jointpos_t w4_jointpos;

	ros::Subscriber task_pos_sub = n.subscribe("tip_goal", 1000, poseCallBack);
	ros::Subscriber task_force_sub = n.subscribe("tip_force", 1000, forceCallBack);
	ros::Subscriber task_torque_sub = n.subscribe("joint_torque", 1000, torqueCallBack);
  	
  	ros::Publisher chatter_pub = n.advertise<sensor_msgs::JointState>("joint_state", 1000);

	rc = initialize_W4_disabled();
	if(rc == 0)
		return 0;
	rc = -1;
	while(rc == -1){
		ROS_INFO("Homing and retrying");
		sendW4(handles[0], W4_CMD_STATE | W4_HOME, W4_RCMD_NULL, NULL, 0, NULL, 0); 
		rc = initialize_W4_disabled();
	}

	while (ros::ok()){
		rc = sendW4(handles[0], W4_CMD_NULL, W4_RCMD_TASKPOS, NULL, 0, &w4_taskpos_ht, sizeof(w4_taskpos_ht)); 
		if(rc < 1){
			ROS_INFO("error reading command ");
		}else{
			transform.setOrigin( tf::Vector3(w4_taskpos_ht.x[0]/1000,w4_taskpos_ht.x[1]/1000,w4_taskpos_ht.x[2]/1000));


			tf::Matrix3x3 rot(w4_taskpos_ht.h[0],w4_taskpos_ht.h[1],w4_taskpos_ht.h[2],
							  w4_taskpos_ht.h[3],w4_taskpos_ht.h[4],w4_taskpos_ht.h[5],
							  w4_taskpos_ht.h[6],w4_taskpos_ht.h[7],w4_taskpos_ht.h[8]);

			transform.setBasis(rot);
    		br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "entact_tip"));

		}
	
		rc = sendW4(handles[0], W4_CMD_NULL, W4_RCMD_JOINTPOS, NULL, 0, &w4_jointpos, sizeof(w4_jointpos)); 
		if(rc < 1){
			ROS_INFO("error reading command ");
		}else {
			js.header.stamp = ros::Time::now();
			for( int i =0; i<8; i ++){
				js.position[i] = w4_jointpos.q[i];
				js.velocity[i] = w4_jointpos.q[7+i];
			}
			rc = sendW4(handles[0], W4_CMD_NULL, W4_RCMD_MOTORTAU, NULL, 0, &tau, sizeof(tau)); 
	  		chatter_pub.publish(js);
			for( int i =0; i<8; i ++){
				js.effort[i] = tau[i];
			}

			if(debug){
				ROS_INFO("jointpos %1.9f %1.3f %1.3f %1.3f %1.3f %1.3f %1.3f %1.3f",w4_jointpos.q[0],w4_jointpos.q[1]
															  ,w4_jointpos.q[2],w4_jointpos.q[3]
															  ,w4_jointpos.q[4],w4_jointpos.q[5]
															  ,w4_jointpos.q[6],w4_jointpos.q[7]);
				
				ROS_INFO("jointtau %1.3f %1.3f %1.3f %1.3f %1.3f %1.3f %1.3f %1.3f",tau[0],tau[1]
												  ,tau[2],tau[3]
												  ,tau[4],tau[5]
												  ,tau[6],tau[7]);
	  		}
	  	}
    ros::spinOnce();
    loop_rate.sleep();
  	}
  	//shut device down
  	sendW4(handles[0], W4_CMD_STATE|W4_DISABLED, W4_RCMD_NULL, NULL, 0, NULL, 0);
	return 0;
}