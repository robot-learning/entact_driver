#!/usr/bin/env python

import rospy
import tf
from tf import TransformListener
import numpy as np
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState

global current 

def jointCallback(data):
   global current
   current = np.array(data.position)



rospy.init_node('circle')

index = 0

pub = rospy.Publisher('joint_torque', Float64MultiArray, queue_size=10)
rospy.Subscriber("joint_state", JointState, jointCallback)

rospy.sleep(0.1)
target = current


kp_m = 2000.0
kd_m = 13.0

kp_small = 10000.0
kd_small = 65.0

kp = np.array([kp_small,kp_small,kp_m,kp_small,kp_small,kp_m,kp_m,kp_m])
kd = np.array([kd_small,kd_small,kd_m,kd_small,kd_small,kd_m,kd_m,kd_m])

dt = 0.001
error = 0
while not rospy.is_shutdown():

   print(current)

   if False and np.sum(error) < 0.005:
      index = index+1 % 4
      error = current - current #just wanted 0 error
      lastError = 0 
      diff = 0
   else:
      lastError = error
      error = target - current
      diff = (error-lastError)*(1/dt)

   torque = np.multiply(error,kp) + np.multiply(diff,kd)

   msg = Float64MultiArray()
   msg.data = torque

   pub.publish(msg)
   rospy.sleep(dt)
