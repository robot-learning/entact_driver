#!/usr/bin/env python

import rospy
import tf
from tf import TransformListener
import numpy as np
from geometry_msgs.msg import TwistStamped

rospy.init_node('circle')

goals = [(0.0,0.0,0,  0,0,0),(0.05,0.05,0),(-0.05,0.05,0),(-0.05,-0.05,0),(0.05,-0.05,0)]
index = 0
tfl = TransformListener()

pub = rospy.Publisher('tip_force', TwistStamped, queue_size=10)
rospy.sleep(0.1)

kp_lin = 6.0
kd_lin = 2.0
kp_ang = 0.03
kd_ang = 0.005

kp = np.array([kp_lin,kp_lin,kp_lin,kp_ang,kp_ang,kp_ang])
kd = np.array([kd_lin,kd_lin,kd_lin,kd_ang,kd_ang,kd_ang])

dt = 0.001
error = 0
while not rospy.is_shutdown():
   (position, orientation) = tfl.lookupTransform('world','entact_tip', rospy.Time(0))
   euler = tf.transformations.euler_from_quaternion(orientation)

   current = np.concatenate([np.array(position),np.array(euler)])
   print(euler)

   target = np.array(goals[index])
   if False and np.sum(error) < 0.005:
      index = index+1 % 4
      error = current - current #just wanted 0 error
      lastError = 0 
      diff = 0
   else:
      lastError = error
      error = target - current
      diff = (error-lastError)*(1/dt)

   force = np.multiply(error,kp) + np.multiply(diff,kd)

   twist = TwistStamped()
   twist.header.stamp = rospy.Time.now()
   twist.header.frame_id = 'world'
   twist.twist.linear.x = force[0]
   twist.twist.linear.y = force[1]
   twist.twist.linear.z = force[2]


   pub.publish(twist)
   rospy.sleep(dt)

twist = TwistStamped()
twist.twist.linear.x = 0
twist.twist.linear.y = 0
twist.twist.linear.z = 0

pub.publish(twist)