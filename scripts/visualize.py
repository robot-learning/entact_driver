#!/usr/bin/env python

from visualization_msgs.msg import Marker
import rospy

topic = 'tip_marker'
publisher = rospy.Publisher(topic, Marker)

rospy.init_node('visualize')

action = Marker.ADD
while not rospy.is_shutdown():


   marker = Marker()
   marker.header.frame_id = "entact_tip"
   marker.type = marker.CYLINDER
   marker.action = action
   action = Marker.MODIFY
   marker.scale.x = 0.08
   marker.scale.y = 0.08
   marker.scale.z = 0.08
   
   marker.color.a = 1.0
   marker.color.b = 1.0

   marker.pose.orientation.w = 1.0
   marker.pose.position.x = 0
   marker.pose.position.y = 0
   marker.pose.position.z = 0


   publisher.publish(marker)
   rospy.sleep(0.01)