
#ifndef _w4_api
#define _w4_api

#include <stdint.h>
#include "w4_packets.h"

extern "C" 
{
	int openW4();
	int closeW4();

	int discoverW4(void *handles[], int length);
	int connectW4(void *handles[], int index, char ip_address[]);

	int getipW4(void * handle, char ip_address[]);

	int sendW4(void *handle, int command, int return_command, void *sbuf, int s_len, void *rbuf, int r_len);
	
};

#endif // _w4_api
