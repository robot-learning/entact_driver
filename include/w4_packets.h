// --------------------------------------------------------------------------------------
// Module       : W4_PACKETS
// Description  : Communication packet structures and commands
// Author       : R. Leslie
// Revision     : 2.0
// --------------------------------------------------------------------------------------
// Entact Robotics inc. www.entactrobotics.com
// --------------------------------------------------------------------------------------
//
// Change Log
// October 21, 2014: RL: Revision 1.0
// December 17, 2014: RL: Revision 2.0
// December 17, 2014: RL: Totally revamped for new W4 API which uses a whole new set of commands, and native single precision (matching the W4 board)

#ifndef __W4_PACKETS_H__
#define __W4_PACKETS_H__

//*****************************************************************************************
// Master - W4_API Packets
//*****************************************************************************************

#pragma pack(push)
#pragma pack(1)

typedef struct // Packet header
{
  uint16_t    command;
  uint16_t    return_command;
  uint16_t    length;	// payload data size (bytes)
} udp_hdr_t;		 

typedef struct // General purpose packet structure
{
  udp_hdr_t   header;
  uint8_t     data[256];
} udp_pkt_t;

typedef struct // General setpoint packet
{
  udp_hdr_t   header;
  float       setpoint[16];
} udp_inpkt_spt_t;

typedef struct // Task space position data 
{
  float       x[3];
  float       h[9];
  float       xd[3];
  float       w[3];
  float       extra[8]; 
} taskpos_t;

typedef struct // Joint space position data
{
  float       q[8];
  float       qd[8];
  float       extra[8]; 
} jointpos_t;

typedef struct // Status data
{
  uint32_t    identifier;
  uint32_t    state;
  uint32_t    homed;
  uint32_t    fault;
  uint32_t    frequency;
} status_t;   

typedef struct // Task space position data packet 
{
  udp_hdr_t   header;
  taskpos_t   data;
} udp_outpkt_taskpos_t; 

typedef struct // Joint space position data packet
{
  udp_hdr_t   header;
  jointpos_t  data; 
} udp_outpkt_jointpos_t; 

typedef struct // Motor torque data packet
{
  udp_hdr_t   header;  
  float       tau[8];
} udp_outpkt_motortau_t;

typedef struct // Status data packet (used for any acknowledgement OR status update)
{
  udp_hdr_t   header; 
  status_t    data;
} udp_outpkt_status_t; 


#pragma pack(pop)

// W4 Commands ***************************************

#define W4_CMD_STATE            (0x0100) // State Packets

#define W4_DISABLED             (0x0000)			
#define W4_FORCE_CONTROL        (0x0001)
#define W4_TORQUE_CONTROL       (0x0002)
#define W4_POSITION_CONTROL     (0x0003)
#define W4_HOME                 (0x0004)
#define W4_JOINT_DAMPING_ON     (0x0005)
#define W4_JOINT_DAMPING_OFF    (0x0006)
#define W4_TASK_DAMPING_ON      (0x0007)
#define W4_TASK_DAMPING_OFF     (0x0008)

#define W4_CMD_SETPOINT         (0x0200) // Setpoint Packets

#define W4_FORCE                (0x0000)
#define W4_TORQUE               (0x0001)
#define W4_POSITION             (0x0002)
#define W4_HOME_OFFSET          (0x0003)
#define W4_JOINT_DAMPING        (0x0004)
#define W4_TASK_DAMPING         (0x0005)
#define W4_MAX_FORCE            (0x0006) // currently not implemented in w4 firmware
#define W4_MAX_TORQUE           (0x0007)
#define W4_MAX_POSITION         (0x0008) // currently not implemented in w4 firmware
#define W4_RT_FREQUENCY         (0x0009) // currently not implemented in w4 firmware
#define W4_WD_TIMEOUT           (0x0010) // currently not implemented in w4 firmware

#define W4_CMD_NULL             (0x0FFF)

// ACK and NACK modifiers *********************************

#define W4_CMD_ACK              (0x0000)
#define W4_CMD_NACK             (0x1000) 

// API Return Commands ************************************

#define W4_RCMD_TASKPOS         (0x0000)
#define W4_RCMD_JOINTPOS        (0x0001)
#define W4_RCMD_MOTORTAU        (0x0002)
#define W4_RCMD_STATUS          (0x0003)

#define W4_RCMD_NULL            (0x0FFF)


#endif /* __W4_PACKETS_H__ */